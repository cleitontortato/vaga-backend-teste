# Vaga Backend
---
Olá Dev, joia? 

Nós da **Usadosbr.com** estamos em expansão e para caminhar junto com a gente nessa evolução, para isso, precisamos de profissionais interessantes e interessados, com boa capacidade de aprendizado, adaptação e compromisso. 

O teste tem como o principal objetivo avaliar e desafiar o seu conhecimento. 

### Desafio

Sua missão é desenvolver uma API para ser utilizada por clientes da UsadosBR.com. Este serviço tem como principal objetivo cadastrar e listar carros. As propriedades necessárias para este cadastro são:

- Marca
- Modelo
- Ano 
- Acessórios
- Foto

É necessário apresentar a Marca sempre em uppercase.\
É necessário validar a quantidade de caractere que esta sendo cadastrado na Marca, não podendo ter mais que cinquenta caracteres, caso ultrapasse o limite permitido, é preciso informar o usuário com uma mensagem amigável.\
É necessário apresentar o Ano no seguinte formato: 2019/2020\
É Obrigatório ter listagens, busca, paginação e filtros.

Para fazer o projeto é necessário utilizar as seguintes tecnologias:

- .NET Core 3.x
- Conceitos de API Restful
- Banco de dados (MySQL ou PostgreSQL)
- GIt

### Interessante
- Docker
- DDD
- TDD

### Por onde começo?

Você pode fazer um fork desse repositório, para sua conta no GitLab, após disso crie uma nova branch com o seu nome (ex: nome_sobrenome). 
Adicionar seu CV em PDF no projeto ou Linkedin no README.

Após terminar o desafio, você pode solicitar um PR (pull request) para a branch master, vamos receber e fazer a avaliação.

Obrigado e boa sorte! ;) 
